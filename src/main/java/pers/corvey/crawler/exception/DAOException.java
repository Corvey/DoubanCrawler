package pers.corvey.crawler.exception;

public class DAOException extends Exception {

    private static final long serialVersionUID = -3219541844942415001L;

    public DAOException() {
        super();
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
