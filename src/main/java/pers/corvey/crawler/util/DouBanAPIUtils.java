package pers.corvey.crawler.util;

import java.lang.reflect.Field;

import pers.corvey.crawler.model.json.BookJson;


/**
 * 豆瓣v2 API工具类
 * @author Corvey
 * @version 1.0.0
 */
public class DouBanAPIUtils {
    
    
    /**
     * 每次搜索可返回的最大结果条数
     */
    public static final int MAX_COUNT = 100;
    
    /**
     * 每次搜索默认返回结果条数
     */
    public static final int DEFAULT_COUNT = 20;
    
    /**
     * 搜索结果需要包含的属性
     */
    protected static final String FIELD_NAMES;
    
    // 根据BookJson类解析出每次搜索需要包含哪些属性
    static {
        StringBuilder sb = new StringBuilder("fields=");
        Field[] fields = BookJson.class.getDeclaredFields();
        for (Field field : fields) {
            sb.append(field.getName());
            sb.append(',');
        }
        sb.deleteCharAt(sb.length() - 1);
        FIELD_NAMES = sb.toString();
    }
    
    
    /**
     * 根据搜索条件构造豆瓣API地址
     * <p>注意：关键词与标签两者必填其一
     * @param q    关键词
     * @param tag 标签
     * @param start    起始位置，默认为0
     * @param count    要求搜索结果条数，默认为 DEFAULT_COUNT
     * @return API地址
     */
    public static String createFindBooksURL(String q, String tag, 
            Integer start, Integer count) {
        final String url = "https://api.douban.com/v2/book/search?";
        StringBuilder sb = new StringBuilder(url);
        boolean isNeedAndSign = false;
        if (q != null && !"".equals(q)) {
            sb.append("q=");
            sb.append(q);
            isNeedAndSign = true;
        }
        if (tag != null && !"".equals(tag)) {
            if (isNeedAndSign)
                sb.append('&');
            else
                isNeedAndSign = true;
            sb.append("tag=");
            sb.append(tag);
        }
        if (start != null) {
            if (isNeedAndSign)
                sb.append('&');
            else
                isNeedAndSign = true;
            sb.append("start=");
            sb.append(start);
        }
        if (count != null) {
            if (isNeedAndSign)
                sb.append('&');
            else
                isNeedAndSign = true;
            sb.append("count=");
            sb.append(count);
        }
        sb.append('&');
        sb.append(FIELD_NAMES);
        return sb.toString();
    }
}
