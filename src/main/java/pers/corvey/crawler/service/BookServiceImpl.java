package pers.corvey.crawler.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pers.corvey.crawler.dao.AuthorDAO;
import pers.corvey.crawler.dao.BookAndTagDAO;
import pers.corvey.crawler.dao.BookDAO;
import pers.corvey.crawler.dao.TagDAO;
import pers.corvey.crawler.exception.DAOException;
import pers.corvey.crawler.model.Author;
import pers.corvey.crawler.model.Book;
import pers.corvey.crawler.model.BookAndTag;
import pers.corvey.crawler.model.Tag;
import pers.corvey.crawler.model.json.BookJson;
import pers.corvey.crawler.transformer.BookTransformer;
import pers.corvey.crawler.util.DouBanAPIUtils;
import pers.corvey.crawler.util.JsonUtils;

/**
 * 图书服务实现类
 * @author Corvey
 * @version 1.0.0
 */
@Service
public class BookServiceImpl implements BookService {

    /**
     * 每次抓取可返回的最大结果条数
     */
    private static final int MAX_COUNT = DouBanAPIUtils.MAX_COUNT;
    
    /**
     * 每次抓取返回的默认条数
     */
    private static final int DEFAULT_COUNT = DouBanAPIUtils.DEFAULT_COUNT;
    
    private static final Logger logger = LoggerFactory.getLogger(BookServiceImpl.class);
    
    @Autowired
    private BookDAO bookDAO;
    
    @Autowired
    private AuthorDAO authorDAO;
    
    @Autowired
    private TagDAO tagDAO;
    
    @Autowired
    private BookAndTagDAO bookAndTagDAO;
    
    @Override
    public int grabBooks(String q, String tag, Integer count) throws IOException {
        int ret = 0;
        if (count == null) {
            count = DEFAULT_COUNT;
        }
        if (count > MAX_COUNT) {
            for (int start = 0; start < count; start += MAX_COUNT) {
                int cnt = start + MAX_COUNT <= count ? MAX_COUNT : count - start;
                String url = DouBanAPIUtils.createFindBooksURL(q, tag, start, cnt);
                List<Book> books = grabBooks(url);
                if (books == null) {
                    break;
                }
                ret += saveBook(books);
            }
        } else {
            String url = DouBanAPIUtils.createFindBooksURL(q, tag, null, count);
            List<Book> books = grabBooks(url);
            if (books != null) {
                ret = saveBook(books);
            }
        }
        return ret;
    }
    
    /**
     * 从URL地址返回的json结果中抓取图书
     * @param url 目标地址
     * @return 抓取到的图书列表
     * @throws IOException
     */
    private List<Book> grabBooks(String url) throws IOException {
        String json = JsonUtils.getJsonStringFromURL(url);
        json = JsonUtils.getField(json, "books");
        if ("[]".equals(json)) {
            return null;
        }
        List<BookJson> bookJsons = JsonUtils.json2List(json, ArrayList.class, BookJson.class);
        List<Book> books = BookTransformer.bookJson2Book(bookJsons);
        return books;
    }
    
    /**
     * 将图书数据插入到数据库
     * @param book 目标图书
     * @return 插入是否成功
     */
    private boolean saveBook(Book book) {
        if (book == null) {
            return false;
        }
        if (!bookDAO.exists(book.getId())) {
            book.setGmtCreate(new Date());
            book.setGmtModified(new Date());
            try {
                setTagId(book.getTags());
                setAuthorId(book.getAuthors());
                bookDAO.save(book);
                saveBookAndTag(book.getId(), book.getTags());
            } catch (Exception e) {
                logger.info("出错图书的地址为：" + book.getDoubanURL());
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
    
    /**
     * 将一组图书数据插入到数据库
     * @param books 一组图书
     * @return 成功插入数量
     */
    private int saveBook(Iterable<Book> books) {
        int sum = 0;
        for (Book book : books) {
            if (saveBook(book)) {
                ++sum;
            }
        }
        return sum;
    }
    
    /**
     * 为一组标签设置id
     * <p>若该标签在数据库中已存在，则其id为数据库中已存在的值；否则将其插入到数据库
     * @param tags 一组标签
     * @throws Exception
     */
    private void setTagId(Iterable<Tag> tags) throws DAOException {
        String id;
        for (Tag tag : tags) {
            id = tagDAO.findByName(tag.getName());
            if (id == null) {
                try {
                    tagDAO.save(tag);
                } catch (Exception e) {
                    logger.info("save Tag出错！\n" + tag.toString());
                    throw new DAOException(e);
                }
            } else {
                tag.setId(id);
            }
        }
    }
    
    /**
     * 为一组作者设置id
     * <p>若该作者在数据库中已存在，则其id为数据库中已存在的值；否则将其插入到数据库
     * @param authors 一组作者 
     * @throws Exception
     */
    private void setAuthorId(Iterable<Author> authors) throws DAOException {
        String id;
        for (Author author : authors) {
            id = authorDAO.findByName(author.getName());
            if (id == null) {
                try {
                    authorDAO.save(author);
                } catch (Exception e) {
                    logger.info("save Author出错！\n" + author.toString());
                    throw new DAOException(e);
                }
            } else {
                author.setId(id);
            }
        }
    }
    
    /**
     * 将图书与标签之间的多对多关系，保存到数据库当中
     * <p>由于图书与标签之间的多对多关系，不仅包含bookId和tagId属性，
     * 还有count属性来记录被该标签标记的次数，因此难以通过ManyToMany注解
     * 来完成多对多关系的数据保存，只能手动实现，若有更好的实现请告知
     * @param bookId 图书id
     * @param tags 一组标签
     * @throws Exception
     * @see BookAndTag
     */
    private void saveBookAndTag(String bookId, Iterable<Tag> tags) throws DAOException {
        BookAndTag bookAndTag = new BookAndTag();
        bookAndTag.setBookId(bookId);
        for (Tag tag : tags) {
            bookAndTag.setTagId(tag.getId());
            bookAndTag.setCount(tag.getCount());
            try {
                bookAndTagDAO.save(bookAndTag);
            } catch (Exception e) {
                logger.info("save BookAndTag出错！\n" + bookAndTag.toString());
                throw new DAOException(e);
            }
        }
    }
}
