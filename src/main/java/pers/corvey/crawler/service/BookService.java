package pers.corvey.crawler.service;

import java.io.IOException;

/**
 * 图书服务类
 * @author Corvey
 * @version 1.0.0
 */
public interface BookService {
    /**
     * 抓取图书并存入数据库
     * @param q 抓取图书的关键词
     * @param tag 抓取图书的标签
     * @param count 抓取数量
     * @return 成功抓取到的图书数量
     * @throws IOException
     */
    int grabBooks(String q, String tag, Integer count) throws IOException;
}
