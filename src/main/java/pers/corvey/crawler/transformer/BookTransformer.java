package pers.corvey.crawler.transformer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pers.corvey.crawler.model.Author;
import pers.corvey.crawler.model.Book;
import pers.corvey.crawler.model.Tag;
import pers.corvey.crawler.model.json.BookJson;
import pers.corvey.crawler.model.json.TagJson;

/**
 * 图书转换器
 * <p>由于API返回的json数据，格式上或者其他方面往往与我们的需求不相符，所以需要一个转换器来进行转换
 * @author Corvey
 * @version 1.0.0
 */
public class BookTransformer {

    /**
     * 将BookJson实例转换为Book实例
     * @param bookJson BookJson实例
     * @return Book实例
     */
    public static Book bookJson2Book(BookJson bookJson) {
        Book book = new Book();
        
        Integer pages = extractInt(bookJson.getPages());
        book.setPages(pages);
        
        Double price = extractDouble(bookJson.getPrice());
        book.setPrice(price);
        
        List<Author> authors = transformAuthors(bookJson.getAuthor());
        book.setAuthors(authors);
        
        Set<Tag> tags = transformTags(bookJson.getTags());
        book.setTags(tags);
        
        book.setScore(bookJson.getRating().getAverage());
        book.setNumRaters(bookJson.getRating().getNumRaters());
        book.setId(bookJson.getId());
        book.setIsbn13(bookJson.getIsbn13());
        book.setTitle(bookJson.getTitle());
        book.setImage(bookJson.getImage());
        book.setPublisher(bookJson.getPublisher());
        book.setPubdate(bookJson.getPubdate());
        book.setBinding(bookJson.getBinding());
        book.setSummary(bookJson.getSummary());
        book.setCatalog(bookJson.getCatalog());
        return book;
    }
    
    /**
     * 将一组BookJson实例转换为一组Book实例
     * @param bookJsons 一组BookJson实例
     * @return 一组Book实例
     */
    public static List<Book> bookJson2Book(Iterable<BookJson> bookJsons) {
        List<Book> books = new ArrayList<>();
        for (BookJson bookJson : bookJsons) {
            books.add(bookJson2Book(bookJson));
        }
        return books;
    }
    
    private static List<Author> transformAuthors(Iterable<String> authorNames) {
        //有些作者名称之间分隔符有问题，导致作者名称过长无法存入数据库，因此需要再分
        final String SPLIT_REGEX = "　|\\^|\\/";
        List<Author> ret = new ArrayList<>();
        for (String name : authorNames) {
            String[] nameInNames = name.split(SPLIT_REGEX);
            for (String nameInName : nameInNames) {
                if (!"".equals(nameInName)) {
                    Author author = new Author();
                    author.setName(nameInName);
                    ret.add(author);
                }
            }
        }
        return ret;
    }
    
    private static Set<Tag> transformTags(Iterable<TagJson> tags) {
        Set<Tag> ret = new HashSet<>();
        for (TagJson tagJson : tags) {
            Tag tag = new Tag();
            tag.setName(tagJson.getName());
            tag.setCount(tagJson.getCount());
            ret.add(tag);
        }
        return ret;
    }
    
    private static Integer extractInt(String str) {
        final String regex = "[1-9]\\d*";
        String intStr = extract(str, regex);
        if (intStr != null) {
            return Integer.parseInt(intStr);
        }
        return null;
    }
    
    private static Double extractDouble(String str) {
        final String regex = "\\d+(\\.\\d+)?";
        String doubleStr = extract(str, regex);
        if (doubleStr != null) {
            return Double.parseDouble(doubleStr);
        }
        return null;
    }
    
    /**
     * 从字符串中提取首个符合正则表达式要求的子串
     * @param str 源字符串
     * @param regex 正则表达式
     * @return 首个符合正则表达式要求的子串
     */
    private static String extract(String str, String regex) {
        Pattern pattern = Pattern.compile(regex);  
        Matcher matcher = pattern.matcher(str);  
        if (matcher.find()) {  
            return matcher.group(0);
        }
        return null;
    }
}
