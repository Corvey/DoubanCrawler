package pers.corvey.crawler.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;

/**
 * BookAndTag的主键类
 * @author Corvey
 * @version 1.0.0
 */
@Embeddable
public class BookAndTagPK implements Serializable {

    private static final long serialVersionUID = -2659412594336697649L;
    private String bookId;
    private String tagId;
    
    public BookAndTagPK() {}
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof BookAndTagPK)) {
            return false;
        }
        BookAndTagPK pkObj = (BookAndTagPK) obj;
        if (Objects.equals(getBookId(), pkObj.getBookId())
                && Objects.equals(getTagId(), pkObj.getTagId())) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(getBookId(), getTagId());
    }
    
    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }
}
