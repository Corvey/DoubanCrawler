package pers.corvey.crawler.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 图书与标签之间的多对多关系记录类
 * <p>由于图书与标签之间的多对多关系，不仅包含bookId和tagId属性，
 * 还有count属性来记录被该标签标记的次数，因此难以由ManyToMany注解来完成
 * 只能借助这个类，若有更好的实现办法请告知
 * @author Corvey
 * @version 1.0.0
 * @see BookAndTagPK
 */
@Entity
@Table(name = "book_tag")
public class BookAndTag {
    
    @EmbeddedId
    private BookAndTagPK id;
    private Integer count;
    
    public BookAndTag() {
        id = new BookAndTagPK();
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    @Column(name = "book_id")
    public String getBookId() {
        return id.getBookId();
    }

    public void setBookId(String bookId) {
        this.id.setBookId(bookId);
    }

    @Column(name = "Tag_id")
    public String getTagId() {
        return id.getTagId();
    }

    public void setTagId(String tagId) {
        this.id.setTagId(tagId);
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
