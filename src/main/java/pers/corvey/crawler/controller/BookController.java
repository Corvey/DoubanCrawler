package pers.corvey.crawler.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import pers.corvey.crawler.model.json.TaskResultJson;
import pers.corvey.crawler.service.BookService;
import pers.corvey.crawler.util.JsonUtils;


/**
 * 图书控制器
 * @author Corvey
 * @version 1.0.0
 */
@Controller
public class BookController {

    private static final Logger logger = LoggerFactory.getLogger(BookController.class);
    
    @Autowired
    private BookService bookService;

    @RequestMapping("/")
    public String index() {
        return "/index.html";
    }
    
    /**
     * 添加抓取任务，仅接受POST请求
     * @param q 抓取关键词，q和tag二者必填其一
     * @param tag 抓取标签名，q和tag二者必填其一
     * @param count 抓取条数，必须大于0
     * @return 以JSON格式返回任务完成情况
     */
    @PostMapping("/tasks")
    @ResponseBody
    public String addTask(String q, String tag, Integer count) {
        TaskResultJson taskResultJson = new TaskResultJson();    // 记录任务完成情况
        int numOfBooks = 0;    // 记录成功抓取到的图书数目
        if ( (q == null || "".equals(q))
                && (tag == null || "".equals(tag)) 
                || count != null && count < 1) {
            taskResultJson.setStatus(false);
            taskResultJson.setMsg("参数不合法！");
        } else {
            long beginTime = System.currentTimeMillis();
            try {
                numOfBooks = bookService.grabBooks(q, tag, count);
                if (numOfBooks == 0) {
                    taskResultJson.setStatus(false);
                    taskResultJson.setMsg("抓取失败！未找到相应图书！");
                    logger.warn(String.format("抓取失败！q=%s, tag=%s, count=%d", q, tag, count));
                } else {
                    taskResultJson.setStatus(true);
                    taskResultJson.setMsg(String.format("成功抓取图书%d本", numOfBooks));
                }
            } catch (IOException e) {
                taskResultJson.setStatus(false);
                taskResultJson.setMsg("IO异常！");
                e.printStackTrace();
            } finally {
                long endTime = System.currentTimeMillis();
                taskResultJson.setTime(endTime - beginTime);
            }
        }
        logger.info(String.format("抓取%d本图书，耗时%d毫秒", numOfBooks, taskResultJson.getTime()));
        return JsonUtils.object2Json(taskResultJson);
    }
}
