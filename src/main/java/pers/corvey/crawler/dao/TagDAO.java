package pers.corvey.crawler.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import pers.corvey.crawler.model.Tag;

public interface TagDAO extends CrudRepository<Tag, String> {

    @Query("select id from Tag where name = ?1")
    String findByName(String name);
    
}
