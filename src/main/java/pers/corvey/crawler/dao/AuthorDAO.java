package pers.corvey.crawler.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import pers.corvey.crawler.model.Author;

public interface AuthorDAO extends CrudRepository<Author, String> {

    @Query("select id from Author where name = ?1")
    String findByName(String name);
    
}
