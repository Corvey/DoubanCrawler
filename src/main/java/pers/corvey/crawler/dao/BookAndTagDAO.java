package pers.corvey.crawler.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pers.corvey.crawler.model.BookAndTag;
import pers.corvey.crawler.model.BookAndTagPK;

@Repository
public interface BookAndTagDAO 
    extends CrudRepository<BookAndTag, BookAndTagPK> {
    
}
