package pers.corvey.crawler.dao;

import org.springframework.data.repository.CrudRepository;

import pers.corvey.crawler.model.Book;

public interface BookDAO extends CrudRepository<Book, String> {

}
