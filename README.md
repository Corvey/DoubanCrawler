# 项目介绍
小型JavaWeb项目，通过豆瓣提供的API，抓取图书并保存到MySQL数据库。
项目主页为http://localhost:8080

## 运行展示
![运行图](https://gitee.com/uploads/images/2017/1015/154348_0f7a3bb7_1524401.png "运行图.png")

## 组织结构
* pers.corvey.crawler
    * controller
        * BookController.java
    * service
        * BookService.java
        * BookserViceImpl.java
    * dao
        * BookDAO.java
        * AuthorDAO.java
        * TagDAO.java
        * BookAndTagDAO.java
    * transformer
        * BookTransformer.java
    * model
        * json
            * BookJson.java
            * RatingJson.java
            * TagJson.java
            * TaskResultJson.java
        * Book.java
        * Author.java
        * Tag.java
        * BookAndTag.java
        * BookAndTagPK.java
    * exception
        * DAOException.java
    * util
        * DouBanAPIUtils.java
        * JsonUtils.java
    * App.java
    * DatasourceConfiguration.java

## 技术选型
* 前端
    * Html/Css/JavaScript
    * Bootstrap
    * Font Awesome
    * jQuery
    * Vue.js
* 后端
    * Java
    * Spring Boot
    * C3P0
    * SLF4J
    * Log4J
    * Jackson
* 数据库
    * MySQL

## 业务流程简述
1. 在浏览器中输入要抓取图书的关键词或标签，以及抓取数量，点击创建。通过jQuery的AJAX将数据传递到服务器。
1. BookController对数据进行检查，然后调用BookService的方法去抓取图书。
1. 先调用DouBanAPIUtils的方法，利用输入的数据，构造出对应的豆瓣API地址。
1. 再调用JsonUtils的方法从该API地址中获取对应图书的json字符串数据，并转换为相应的json类实例。
1. 再调用BookTransformer的方法，根据不同的转换规则，将json类实例转换成Book类实例。
1. 将Book类实例的数据插入到MySQL数据库中。
1. 返回成功抓取到的图书数量，并在浏览器中显示任务完成情况。

## 数据模型
![数据模型图](https://gitee.com/uploads/images/2017/1015/154408_18d79d88_1524401.png "数据库实体关系图.png")
