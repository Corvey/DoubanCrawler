CREATE DATABASE douban_book;
USE douban_book;

CREATE TABLE `book` (
  `id` bigint(20) unsigned NOT NULL,	# id由豆瓣提供
  `isbn13` char(13) DEFAULT NULL COMMENT 'ISBN13',
  `title` varchar(100) NOT NULL COMMENT '书名',
  `image` varchar(150) DEFAULT NULL COMMENT 'image_url',
  `publisher` varchar(50) DEFAULT NULL COMMENT '出版社',
  `pubdate` varchar(20) DEFAULT NULL COMMENT '出版时间',
  `score` decimal(3,1) DEFAULT NULL COMMENT '评分',
  `num_raters` int(11) DEFAULT NULL COMMENT '评价人数',
  `binding` varchar(30) DEFAULT NULL COMMENT '装订方式',
  `price` decimal(5,1) DEFAULT NULL COMMENT '价格',
  `pages` int(10) DEFAULT NULL COMMENT '页数',
  `summary` text COMMENT '摘要',
  `catalog` text COMMENT '目录',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='书本表';

CREATE TABLE `author` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '作者名',
  PRIMARY KEY (`id`),
  UNIQUE KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作者表';

CREATE TABLE `tag` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '标签名',
  PRIMARY KEY (`id`),
  UNIQUE KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签表';

CREATE TABLE `book_author` (
  `book_id` bigint(20) unsigned NOT NULL COMMENT '书号',
  `author_id` bigint(20) unsigned NOT NULL COMMENT '作者号',
  PRIMARY KEY (`book_id`,`author_id`),
  FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`author_id`) REFERENCES `author`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='书本作者表';

CREATE TABLE `book_tag` (
  `book_id` bigint(20) unsigned NOT NULL COMMENT '书号',
  `tag_id` bigint(20) unsigned NOT NULL COMMENT '标签号',
  `count` int(10) unsigned NOT NULL COMMENT '标记次数',
  PRIMARY KEY (`book_id`,`tag_id`),
  FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='书本标签表';
